FROM postgres:16-alpine

LABEL maintainer="Dennis Vesterlund dennis@vestern.se"

ENV MAJOR 16
ENV MINOR 1

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN apk add --update --no-cache py3-psycopg2
